import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App/App';

ReactDOM.render(
  <React.StrictMode>
    <App className='app'/>
  </React.StrictMode>,
  document.getElementById('root')
);