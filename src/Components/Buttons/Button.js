import React from 'react';
import './Buttons.css'

function Button(props) {
  return (
    <div className="buttons">
        {<button className="btn start" onClick={props.start}>Start</button> }
        {<button className="btn stop" onClick={props.stop}>Stop</button> }
        {<button className="btn clear" onClick={props.clear}>Clear</button> }
     
    </div>
  );
}

export default Button;