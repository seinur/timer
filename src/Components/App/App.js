import React, {useState} from 'react';
import Timer from '../Timer/Timer';
import Buttons from '../Buttons/Button';
import './App.css';

function App() {
  const [time, setTime] = useState({ms:0, s:0, m:0, h:0});
  const [interv, setInterv] = useState();


  const start = () => {
    run();
    setInterv(setInterval(run, 10));
  };

  var updatedMs = time.ms, updatedS = time.s, updatedM = time.m, updatedH = time.h;

  const run = () => {
    if(updatedM === 60){
      updatedH++;
      updatedM = 0;
    }
    if(updatedS === 60){
      updatedM++;
      updatedS = 0;
    }
    if(updatedMs === 100){
      updatedS++;
      updatedMs = 0;
    }
    updatedMs++;
    return setTime({ms:updatedMs, s:updatedS, m:updatedM, h:updatedH});
  };

  const stop = () => {
    clearInterval(interv);
  };

  const clear = () => {
    clearInterval(interv);
    setTime({ms:0, s:0, m:0, h:0})
  };




  return (
    
          <div className="app">
               <Timer time={time}/>
               <Buttons  clear={clear} stop={stop} start={start}/>
          </div>
     
  );
}

export default App;